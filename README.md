# OnMyShelf import module for Brickset.com

Module OnMyShelf for import LEGO sets from https://brickset.com

# Installation
Go into `modules/import` and clone this repository:
```bash
git clone https://gitlab.com/onmyshelf/modules/import/brickset
```

# Update
Go into `modules/import/brickset` and run:
```bash
git pull
```

# Help
Need help on this module? [Create an issue here](https://gitlab.com/onmyshelf/modules/import/brickset/-/issues).

More information about OnMyShelf here: https://onmyshelf.app
