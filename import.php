<?php

require_once('inc/import/global/html.php');

class Import extends HtmlImport
{
    public function load()
    {
        $this->website = 'https://brickset.com';

        $this->properties = [
            'source',
            'name',
            'reference',
            'image',
            'theme',
            'year',
            'pieces',
        ];

        return true;
    }


    /**
     * Search item
     * @param  string $search
     * @return array
     */
    public function search($search)
    {
        $this->source = $this->website."/sets?query=".preg_replace('/\s/', '+', $search);
        $this->loadHtml();

        $results = [];
        foreach ($this->html->find('article.set') as $item) {
            $results[] = [
                'source' => $this->website.$this->getLink('div.meta h1 a', $item),
                'name' => $this->getText($this->getHtml('div.meta h1', $item)),
                'image' => $this->getImgSrc('a.highslide img', $item),
                'description' => $this->getText($this->getHtml('div.tags', $item)),
            ];
        }

        return $results;
    }


    /**
     * Get item data from source
     * @return array
     */
    public function getData()
    {
        $this->loadHtml();

        $properties = [
            'source' => $this->source,
            'name' => $this->getDetails('Name'),
            'reference' => $this->getDetails('Number'),
            'image' => $this->download($this->getHtml('meta[property="og:image"]', null, 'content')),
            'theme' => $this->getDetails('Theme'),
            'year' => $this->getDetails("Year released"),
            'pieces' => $this->getDetails("Pieces"),
        ];

        return $properties;
    }


    /**
     * Get item details from label
     * @param  string $label
     * @return string
     */
    private function getDetails($label)
    {
        // parse all item details
        foreach ($this->html->find('section.featurebox dt') as $detail) {
            // if label matches, return value
            if ($this->getText($detail) == $label) {
                return $this->getText($detail->nextSibling());
            }
        }
    }
}
