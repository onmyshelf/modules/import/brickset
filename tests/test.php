<?php

$module = "brickset";

testModuleImportHtmlSearch($module, "tie fighter");

testModuleImportHtmlData($module, "/sets/75376-1/Tantive-IV", [
    'reference' => '/^75376-1$/',
    'pieces' => '/^654$/',
    'theme' => '/Star Wars/',
]);
